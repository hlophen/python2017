# -*- coding: utf-8 -*-
"""
Creator: your name
Last updated: when?

"""

import math  # a python module with mathematics operations


"""

Python program to print a hello message, 
introduce variables, conditionals and lists.
Then displays a goodbye message.

Note, this is a multiline comment
"""

print("Hello world")

a = 1
b = 2
c = 3
x = 2

y = a*x**2 + b*x + c

if x%2 == 0:
    print("x =", x, "is even")
else:
    print("x =", x, "is odd")

primes = [2,3,5,7,11]

for p in primes:
    print p

print("Goodbye")
